package io.easyspring.basis.rbac.model;

import com.fasterxml.jackson.annotation.JsonView;
import io.easyspring.framework.base.model.BaseModel;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;

/**
 * 用户-角色
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "sys_user_role")
@ApiModel(value="用户-角色", description="用户-角色")
public class UserRole extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 用户外键
     */
    @ApiModelProperty(value = "用户外键")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long userId;

    /**
     * 角色外键
     */
    @ApiModelProperty(value = "角色外键")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long roleId;


}