package io.easyspring.basis.rbac.model;

import com.fasterxml.jackson.annotation.JsonView;
import io.easyspring.framework.base.model.BaseModel;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;

/**
 * 角色
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "sys_role")
@ApiModel(value="角色", description="角色")
public class Role extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    @JsonView(CommonJsonView.SimpleView.class)
    private String name;

    /**
     * 角色识别码
     */
    @ApiModelProperty(value = "角色识别码")
    @JsonView(CommonJsonView.SimpleView.class)
    private String code;

    /**
     * 角色简介
     */
    @ApiModelProperty(value = "角色简介")
    @JsonView(CommonJsonView.SimpleView.class)
    private String blurb;

    /**
     * 排序值
     */
    @ApiModelProperty(value = "排序值")
    @JsonView(CommonJsonView.SimpleView.class)
    private Integer easySort;


}