package io.easyspring.basis.rbac.model;

import com.fasterxml.jackson.annotation.JsonView;
import io.easyspring.framework.base.model.BaseModel;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;

/**
 * 角色-权限
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "sys_role_permission")
@ApiModel(value="角色-权限", description="角色-权限")
public class RolePermission extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 角色外键
     */
    @ApiModelProperty(value = "角色外键")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long roleId;

    /**
     * 资源外键
     */
    @ApiModelProperty(value = "资源外键")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long permissionId;


}