package io.easyspring.basis.rbac.model;

import com.fasterxml.jackson.annotation.JsonView;
import io.easyspring.framework.base.model.BaseModel;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.util.Date;

/**
 * 用户
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:08
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "sys_user")
@ApiModel(value="用户", description="用户")
public class User extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @JsonView(CommonJsonView.SimpleView.class)
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    @JsonView(CommonJsonView.SimpleView.class)
    private String password;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    @JsonView(CommonJsonView.SimpleView.class)
    private String nickname;

    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid")
    @JsonView(CommonJsonView.SimpleView.class)
    private String openId;

    /**
     * 头像(文件表外键)
     */
    @ApiModelProperty(value = "头像(文件表外键)")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long photo;

    /**
     * 微信头像连接地址
     */
    @ApiModelProperty(value = "微信头像连接地址")
    @JsonView(CommonJsonView.SimpleView.class)
    private String headImgUrl;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    @JsonView(CommonJsonView.SimpleView.class)
    private String email;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    @JsonView(CommonJsonView.SimpleView.class)
    private String telephone;

    /**
     * 用户类型(“ADMINISTRATOR”, “CLIENT”, “BROKER”,”CLIENT_MANAGER”)(管理员, 客户, 经纪人, 客户经理)
     */
    @ApiModelProperty(value = "用户类型(“ADMINISTRATOR”, “CLIENT”, “BROKER”,”CLIENT_MANAGER”)(管理员, 客户, 经纪人, 客户经理)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String userType;

    /**
     * 用户来源enum('移动端注册','后台注册')
     */
    @ApiModelProperty(value = "用户来源enum('移动端注册','后台注册')")
    @JsonView(CommonJsonView.SimpleView.class)
    private String userSource;

    /**
     * 性别(0:男 1:女)
     */
    @ApiModelProperty(value = "性别(0:男 1:女)")
    @JsonView(CommonJsonView.SimpleView.class)
    private Boolean sex;

    /**
     * 生日
     */
    @ApiModelProperty(value = "生日")
    @JsonView(CommonJsonView.SimpleView.class)
    private Date birthday;

    /**
     * 用户是否已锁定(默认值:0未锁定)
     */
    @ApiModelProperty(value = "用户是否已锁定(默认值:0未锁定)")
    @JsonView(CommonJsonView.SimpleView.class)
    private Boolean locked;


}