package io.easyspring.basis.rbac.model;

import com.fasterxml.jackson.annotation.JsonView;
import io.easyspring.framework.base.model.BaseModel;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;

/**
 * 资源
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "sys_permission")
@ApiModel(value="资源", description="资源")
public class Permission extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 父级外键
     */
    @ApiModelProperty(value = "父级外键")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long parentId;

    /**
     * 权限组别(用于提供不同位置的权限服务)
     */
    @ApiModelProperty(value = "权限组别(用于提供不同位置的权限服务)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String permissionGroup;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    @JsonView(CommonJsonView.SimpleView.class)
    private String name;

    /**
     * 资源类型(“BUTTON”, “MENU”)(按钮, 菜单)
     */
    @ApiModelProperty(value = "资源类型(“BUTTON”, “MENU”)(按钮, 菜单)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String type;

    /**
     * 权限识别码(模块:子模块:功能)
     */
    @ApiModelProperty(value = "权限识别码(模块:子模块:功能)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String code;

    /**
     * 连接地址
     */
    @ApiModelProperty(value = "连接地址")
    @JsonView(CommonJsonView.SimpleView.class)
    private String href;

    /**
     * 路由(供前端使用)
     */
    @ApiModelProperty(value = "路由(供前端使用)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String route;

    /**
     * 请求类型 (“GET”,”POST”,”PUT”,”DELETE”)
     */
    @ApiModelProperty(value = "请求类型 (“GET”,”POST”,”PUT”,”DELETE”)")
    @JsonView(CommonJsonView.SimpleView.class)
    private String requestMethod;

    /**
     * 页面标签显示目标
     */
    @ApiModelProperty(value = "页面标签显示目标")
    @JsonView(CommonJsonView.SimpleView.class)
    private String pageTarget;

    /**
     * 图标
     */
    @ApiModelProperty(value = "图标")
    @JsonView(CommonJsonView.SimpleView.class)
    private String icon;

    /**
     * 缩略图(文件表外键)
     */
    @ApiModelProperty(value = "缩略图(文件表外键)")
    @JsonView(CommonJsonView.SimpleView.class)
    private Long thumbnail;

    /**
     * 排序值
     */
    @ApiModelProperty(value = "排序值")
    @JsonView(CommonJsonView.SimpleView.class)
    private Integer easySort;

    /**
     * 是否显示
     */
    @ApiModelProperty(value = "是否显示")
    @JsonView(CommonJsonView.SimpleView.class)
    private Boolean isShow;

    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    @JsonView(CommonJsonView.SimpleView.class)
    private String brief;


}