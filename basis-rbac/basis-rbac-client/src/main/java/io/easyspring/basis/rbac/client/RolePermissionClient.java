package io.easyspring.basis.rbac.client;

import io.easyspring.basis.rbac.client.fallback.RolePermissionClientFallback;
import io.easyspring.basis.rbac.model.RolePermission;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色-权限 服务类 API
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@FeignClient(value = "basis-rbac-api", fallback = RolePermissionClientFallback.class)
@Validated
public interface RolePermissionClient {

    /**
     * 角色-权限 的添加方法
     *
     * @param rolePermission 角色-权限
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PostMapping("/rbac/role_permission")
    RolePermission save(@NotNull(message = "角色-权限 不能为空") @Valid
                    @RequestBody RolePermission rolePermission);

    /**
     * 根据传入的 id 删除对应的 角色-权限
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @DeleteMapping("/rbac/role_permission/{id:\\d+}")
    boolean delete(@PathVariable(value = "id") Long id);

    /**
     * 角色-权限 的修改方法
     *
     * @param rolePermission 角色-权限
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PutMapping("/rbac/role_permission")
    RolePermission update(@NotNull(message = "角色-权限不能为空") @Valid
                      @RequestBody RolePermission rolePermission);

    /**
     * 根据 id 查询单个 角色-权限
     *
     * @param id 角色-权限 id
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/role_permission/{id:\\d+}")
    RolePermission get(@PathVariable(value = "id") Long id);

    /**
     * 根据条件, 查询 角色-权限 集合信息
     *
     * @param rolePermission 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.RolePermission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/role_permission")
    List<RolePermission> list(RolePermission rolePermission);

    /**
     * 查询 角色-权限 分页信息
     *
     * @param rolePermission 角色-权限 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.RolePermission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/role_permission/page")
    PageInfo<RolePermission> findPage(RolePermission rolePermission, Pageable pageable);
}
