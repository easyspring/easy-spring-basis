package io.easyspring.basis.rbac.client;

import io.easyspring.basis.rbac.client.fallback.UserRoleClientFallback;
import io.easyspring.basis.rbac.model.UserRole;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户-角色 服务类 API
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@FeignClient(value = "basis-rbac-api", fallback = UserRoleClientFallback.class)
@Validated
public interface UserRoleClient {

    /**
     * 用户-角色 的添加方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PostMapping("/rbac/user_role")
    UserRole save(@NotNull(message = "用户-角色 不能为空") @Valid
                    @RequestBody UserRole userRole);

    /**
     * 根据传入的 id 删除对应的 用户-角色
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @DeleteMapping("/rbac/user_role/{id:\\d+}")
    boolean delete(@PathVariable(value = "id") Long id);

    /**
     * 用户-角色 的修改方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PutMapping("/rbac/user_role")
    UserRole update(@NotNull(message = "用户-角色不能为空") @Valid
                      @RequestBody UserRole userRole);

    /**
     * 根据 id 查询单个 用户-角色
     *
     * @param id 用户-角色 id
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/user_role/{id:\\d+}")
    UserRole get(@PathVariable(value = "id") Long id);

    /**
     * 根据条件, 查询 用户-角色 集合信息
     *
     * @param userRole 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/user_role")
    List<UserRole> list(UserRole userRole);

    /**
     * 查询 用户-角色 分页信息
     *
     * @param userRole 用户-角色 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/user_role/page")
    PageInfo<UserRole> findPage(UserRole userRole, Pageable pageable);
}
