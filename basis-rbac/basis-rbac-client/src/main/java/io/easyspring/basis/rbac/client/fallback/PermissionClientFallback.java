package io.easyspring.basis.rbac.client.fallback;

import io.easyspring.basis.rbac.client.PermissionClient;
import io.easyspring.basis.rbac.model.Permission;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 资源 服务降级返回信息的实现
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@Component
@Slf4j
public class PermissionClientFallback implements PermissionClient {

    /**
     * 添加服务出现服务降级时调用的方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public Permission save(Permission permission) {
        log.warn("添加服务出现服务降级, 传入的参数 permission: {}", permission);
        return null;
    }

    /**
     * 删除服务出现服务降级时调用的方法
     *
     * @param id 资源 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public boolean delete(Long id) {
        log.warn("删除服务出现服务降级, 传入的参数 id: {}", id);
        return false;
    }

    /**
     * 修改服务出现服务降级时调用的方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public Permission update(Permission permission) {
        log.warn("修改服务出现服务降级, 传入的参数 permission: {}", permission);
        return null;
    }

    /**
     * 查询服务出现服务降级时调用的方法
     *
     * @param id 资源 id
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public Permission get(Long id) {
        log.warn("查询服务出现服务降级, 传入的参数 id: {}", id);
        return null;
    }

    /**
     * 集合查询服务出现服务降级时调用的方法
     *
     * @param permission 资源
     * @return java.util.List<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public List<Permission> list(Permission permission) {
        log.warn("集合查询服务出现服务降级, 传入的参数 permission: {}", permission);
        return null;
    }

    /**
     * 分页查询服务出现服务降级时调用的方法
     *
     * @param permission 资源
     * @param pageable 分页条件
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public PageInfo<Permission> findPage(Permission permission, Pageable pageable) {
        log.warn("分页查询服务出现服务降级, 传入的参数 permission: {}, pageable: {}",
                permission, pageable);
        return null;
    }
}
