package io.easyspring.basis.rbac.client.fallback;

import io.easyspring.basis.rbac.client.UserRoleClient;
import io.easyspring.basis.rbac.model.UserRole;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户-角色 服务降级返回信息的实现
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@Component
@Slf4j
public class UserRoleClientFallback implements UserRoleClient {

    /**
     * 添加服务出现服务降级时调用的方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public UserRole save(UserRole userRole) {
        log.warn("添加服务出现服务降级, 传入的参数 userRole: {}", userRole);
        return null;
    }

    /**
     * 删除服务出现服务降级时调用的方法
     *
     * @param id 用户-角色 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public boolean delete(Long id) {
        log.warn("删除服务出现服务降级, 传入的参数 id: {}", id);
        return false;
    }

    /**
     * 修改服务出现服务降级时调用的方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public UserRole update(UserRole userRole) {
        log.warn("修改服务出现服务降级, 传入的参数 userRole: {}", userRole);
        return null;
    }

    /**
     * 查询服务出现服务降级时调用的方法
     *
     * @param id 用户-角色 id
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public UserRole get(Long id) {
        log.warn("查询服务出现服务降级, 传入的参数 id: {}", id);
        return null;
    }

    /**
     * 集合查询服务出现服务降级时调用的方法
     *
     * @param userRole 用户-角色
     * @return java.util.List<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public List<UserRole> list(UserRole userRole) {
        log.warn("集合查询服务出现服务降级, 传入的参数 userRole: {}", userRole);
        return null;
    }

    /**
     * 分页查询服务出现服务降级时调用的方法
     *
     * @param userRole 用户-角色
     * @param pageable 分页条件
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public PageInfo<UserRole> findPage(UserRole userRole, Pageable pageable) {
        log.warn("分页查询服务出现服务降级, 传入的参数 userRole: {}, pageable: {}",
                userRole, pageable);
        return null;
    }
}
