package io.easyspring.basis.rbac.client;

import io.easyspring.basis.rbac.client.fallback.RoleClientFallback;
import io.easyspring.basis.rbac.model.Role;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色 服务类 API
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@FeignClient(value = "basis-rbac-api", fallback = RoleClientFallback.class)
@Validated
public interface RoleClient {

    /**
     * 角色 的添加方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @PostMapping("/rbac/role")
    Role save(@NotNull(message = "角色 不能为空") @Valid
                    @RequestBody Role role);

    /**
     * 根据传入的 id 删除对应的 角色
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @DeleteMapping("/rbac/role/{id:\\d+}")
    boolean delete(@PathVariable(value = "id") Long id);

    /**
     * 角色 的修改方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @PutMapping("/rbac/role")
    Role update(@NotNull(message = "角色不能为空") @Valid
                      @RequestBody Role role);

    /**
     * 根据 id 查询单个 角色
     *
     * @param id 角色 id
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @GetMapping("/rbac/role/{id:\\d+}")
    Role get(@PathVariable(value = "id") Long id);

    /**
     * 根据条件, 查询 角色 集合信息
     *
     * @param role 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @GetMapping("/rbac/role")
    List<Role> list(Role role);

    /**
     * 查询 角色 分页信息
     *
     * @param role 角色 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @GetMapping("/rbac/role/page")
    PageInfo<Role> findPage(Role role, Pageable pageable);
}
