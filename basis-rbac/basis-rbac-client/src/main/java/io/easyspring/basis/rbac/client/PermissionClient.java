package io.easyspring.basis.rbac.client;

import io.easyspring.basis.rbac.client.fallback.PermissionClientFallback;
import io.easyspring.basis.rbac.model.Permission;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 资源 服务类 API
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@FeignClient(value = "basis-rbac-api", fallback = PermissionClientFallback.class)
@Validated
public interface PermissionClient {

    /**
     * 资源 的添加方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PostMapping("/rbac/permission")
    Permission save(@NotNull(message = "资源 不能为空") @Valid
                    @RequestBody Permission permission);

    /**
     * 根据传入的 id 删除对应的 资源
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @DeleteMapping("/rbac/permission/{id:\\d+}")
    boolean delete(@PathVariable(value = "id") Long id);

    /**
     * 资源 的修改方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @PutMapping("/rbac/permission")
    Permission update(@NotNull(message = "资源不能为空") @Valid
                      @RequestBody Permission permission);

    /**
     * 根据 id 查询单个 资源
     *
     * @param id 资源 id
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/permission/{id:\\d+}")
    Permission get(@PathVariable(value = "id") Long id);

    /**
     * 根据条件, 查询 资源 集合信息
     *
     * @param permission 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/permission")
    List<Permission> list(Permission permission);

    /**
     * 查询 资源 分页信息
     *
     * @param permission 资源 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping("/rbac/permission/page")
    PageInfo<Permission> findPage(Permission permission, Pageable pageable);
}
