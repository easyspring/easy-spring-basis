package io.easyspring.basis.rbac.client;

import io.easyspring.basis.rbac.client.fallback.UserClientFallback;
import io.easyspring.basis.rbac.model.User;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户 服务类 API
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:08
 */
@FeignClient(value = "basis-rbac-api", fallback = UserClientFallback.class)
@Validated
public interface UserClient {

    /**
     * 用户 的添加方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @PostMapping("/rbac/user")
    User save(@NotNull(message = "用户 不能为空") @Valid
                    @RequestBody User user);

    /**
     * 根据传入的 id 删除对应的 用户
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @DeleteMapping("/rbac/user/{id:\\d+}")
    boolean delete(@PathVariable(value = "id") Long id);

    /**
     * 用户 的修改方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @PutMapping("/rbac/user")
    User update(@NotNull(message = "用户不能为空") @Valid
                      @RequestBody User user);

    /**
     * 根据 id 查询单个 用户
     *
     * @param id 用户 id
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @GetMapping("/rbac/user/{id:\\d+}")
    User get(@PathVariable(value = "id") Long id);

    /**
     * 根据条件, 查询 用户 集合信息
     *
     * @param user 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @GetMapping("/rbac/user")
    List<User> list(User user);

    /**
     * 查询 用户 分页信息
     *
     * @param user 用户 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @GetMapping("/rbac/user/page")
    PageInfo<User> findPage(User user, Pageable pageable);
}
