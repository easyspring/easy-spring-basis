package io.easyspring.basis.rbac.client.fallback;

import io.easyspring.basis.rbac.client.UserClient;
import io.easyspring.basis.rbac.model.User;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户 服务降级返回信息的实现
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:08
 */
@Component
@Slf4j
public class UserClientFallback implements UserClient {

    /**
     * 添加服务出现服务降级时调用的方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public User save(User user) {
        log.warn("添加服务出现服务降级, 传入的参数 user: {}", user);
        return null;
    }

    /**
     * 删除服务出现服务降级时调用的方法
     *
     * @param id 用户 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public boolean delete(Long id) {
        log.warn("删除服务出现服务降级, 传入的参数 id: {}", id);
        return false;
    }

    /**
     * 修改服务出现服务降级时调用的方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public User update(User user) {
        log.warn("修改服务出现服务降级, 传入的参数 user: {}", user);
        return null;
    }

    /**
     * 查询服务出现服务降级时调用的方法
     *
     * @param id 用户 id
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public User get(Long id) {
        log.warn("查询服务出现服务降级, 传入的参数 id: {}", id);
        return null;
    }

    /**
     * 集合查询服务出现服务降级时调用的方法
     *
     * @param user 用户
     * @return java.util.List<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public List<User> list(User user) {
        log.warn("集合查询服务出现服务降级, 传入的参数 user: {}", user);
        return null;
    }

    /**
     * 分页查询服务出现服务降级时调用的方法
     *
     * @param user 用户
     * @param pageable 分页条件
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @Override
    public PageInfo<User> findPage(User user, Pageable pageable) {
        log.warn("分页查询服务出现服务降级, 传入的参数 user: {}, pageable: {}",
                user, pageable);
        return null;
    }
}
