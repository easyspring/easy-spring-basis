package io.easyspring.basis.rbac.client.fallback;

import io.easyspring.basis.rbac.client.RoleClient;
import io.easyspring.basis.rbac.model.Role;
import io.easyspring.framework.base.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色 服务降级返回信息的实现
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@Component
@Slf4j
public class RoleClientFallback implements RoleClient {

    /**
     * 添加服务出现服务降级时调用的方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public Role save(Role role) {
        log.warn("添加服务出现服务降级, 传入的参数 role: {}", role);
        return null;
    }

    /**
     * 删除服务出现服务降级时调用的方法
     *
     * @param id 角色 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public boolean delete(Long id) {
        log.warn("删除服务出现服务降级, 传入的参数 id: {}", id);
        return false;
    }

    /**
     * 修改服务出现服务降级时调用的方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public Role update(Role role) {
        log.warn("修改服务出现服务降级, 传入的参数 role: {}", role);
        return null;
    }

    /**
     * 查询服务出现服务降级时调用的方法
     *
     * @param id 角色 id
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public Role get(Long id) {
        log.warn("查询服务出现服务降级, 传入的参数 id: {}", id);
        return null;
    }

    /**
     * 集合查询服务出现服务降级时调用的方法
     *
     * @param role 角色
     * @return java.util.List<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public List<Role> list(Role role) {
        log.warn("集合查询服务出现服务降级, 传入的参数 role: {}", role);
        return null;
    }

    /**
     * 分页查询服务出现服务降级时调用的方法
     *
     * @param role 角色
     * @param pageable 分页条件
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public PageInfo<Role> findPage(Role role, Pageable pageable) {
        log.warn("分页查询服务出现服务降级, 传入的参数 role: {}, pageable: {}",
                role, pageable);
        return null;
    }
}
