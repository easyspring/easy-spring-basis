package io.easyspring.basis.rbac.mapper;

import io.easyspring.basis.rbac.model.Role;
import io.easyspring.framework.base.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 角色 Mapper 接口
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@Mapper
@Component
public interface RoleMapper extends BaseMapper<Role> {

}
