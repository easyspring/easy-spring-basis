package io.easyspring.basis.rbac.mapper;

import io.easyspring.basis.rbac.model.User;
import io.easyspring.framework.base.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 用户 Mapper 接口
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:08
 */
@Mapper
@Component
public interface UserMapper extends BaseMapper<User> {

}
