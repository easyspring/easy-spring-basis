package io.easyspring.basis.rbac.service;

import io.easyspring.basis.rbac.model.Permission;
import io.easyspring.framework.base.service.BaseService;
import com.github.pagehelper.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

/**
 * 资源 服务类
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@Validated
public interface PermissionService extends BaseService<Permission> {

    /**
     * 根据传入的关键字和分页信息, 查询资源的分页数据
     *
     * @param keyWord 关键字
     * @param pageable 分页信息
     * @return com.github.pagehelper.Page<Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    Page<Permission> findPageBuilder(String keyWord, Pageable pageable);
}
