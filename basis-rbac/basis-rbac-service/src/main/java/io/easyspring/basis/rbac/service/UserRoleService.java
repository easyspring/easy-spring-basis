package io.easyspring.basis.rbac.service;

import io.easyspring.basis.rbac.model.UserRole;
import io.easyspring.framework.base.service.BaseService;
import com.github.pagehelper.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

/**
 * 用户-角色 服务类
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@Validated
public interface UserRoleService extends BaseService<UserRole> {

    /**
     * 根据传入的关键字和分页信息, 查询用户-角色的分页数据
     *
     * @param keyWord 关键字
     * @param pageable 分页信息
     * @return com.github.pagehelper.Page<UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    Page<UserRole> findPageBuilder(String keyWord, Pageable pageable);
}
