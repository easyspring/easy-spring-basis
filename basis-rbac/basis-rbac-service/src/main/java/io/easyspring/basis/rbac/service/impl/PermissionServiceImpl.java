package io.easyspring.basis.rbac.service.impl;

import io.easyspring.basis.rbac.model.Permission;
import io.easyspring.basis.rbac.mapper.PermissionMapper;
import io.easyspring.basis.rbac.service.PermissionService;
import io.easyspring.framework.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import com.github.pagehelper.Page;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.weekend.Weekend;
import tk.mybatis.mapper.weekend.WeekendCriteria;

import javax.annotation.Resource;

/**
 * 资源 服务实现类
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@Service
public class PermissionServiceImpl extends BaseServiceImpl<PermissionMapper, Permission>
        implements PermissionService {

    /**
     * 注入资源的 mapper
     */
    @Resource
    private PermissionMapper permissionMapper;

    /**
     * 根据传入的关键字和分页信息, 查询资源的分页数据
     *
     * @param keyWord 关键字
     * @param pageable 分页信息
     * @return com.github.pagehelper.Page<Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @Override
    public Page<Permission> findPageBuilder(String keyWord, Pageable pageable) {
        // 创建查询条件
        Weekend<Permission> weekend = Weekend.of(Permission.class);

        // 封装查询条件 (关键数据的匹配)
        WeekendCriteria<Permission, Object> weekendEqualCriteria = weekend.weekendCriteria();
        weekendEqualCriteria.andEqualTo(Permission::getDeleted, Permission.DELETED_FALSE);

        // 关键字匹配
        if (!StringUtils.isEmpty(keyWord)) {
            // 封装查询信息(关键字的模糊匹配)
            WeekendCriteria<Permission, Object> weekendKeywordCriteria = weekend.weekendCriteria();
            // TODO 待实现
            // 查询条件封装
            //weekendKeywordCriteria
            //    .orLike(User::getTelephone, WeekendCriteriaUtils.buildLikeParameter(keyWord));
            weekend.and(weekendKeywordCriteria);
        }
        return findPage(weekend, pageable);
    }
}
