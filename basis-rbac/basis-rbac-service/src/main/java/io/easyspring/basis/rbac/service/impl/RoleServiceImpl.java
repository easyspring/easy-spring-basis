package io.easyspring.basis.rbac.service.impl;

import io.easyspring.basis.rbac.model.Role;
import io.easyspring.basis.rbac.mapper.RoleMapper;
import io.easyspring.basis.rbac.service.RoleService;
import io.easyspring.framework.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import com.github.pagehelper.Page;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.weekend.Weekend;
import tk.mybatis.mapper.weekend.WeekendCriteria;

import javax.annotation.Resource;

/**
 * 角色 服务实现类
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role>
        implements RoleService {

    /**
     * 注入角色的 mapper
     */
    @Resource
    private RoleMapper roleMapper;

    /**
     * 根据传入的关键字和分页信息, 查询角色的分页数据
     *
     * @param keyWord 关键字
     * @param pageable 分页信息
     * @return com.github.pagehelper.Page<Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @Override
    public Page<Role> findPageBuilder(String keyWord, Pageable pageable) {
        // 创建查询条件
        Weekend<Role> weekend = Weekend.of(Role.class);

        // 封装查询条件 (关键数据的匹配)
        WeekendCriteria<Role, Object> weekendEqualCriteria = weekend.weekendCriteria();
        weekendEqualCriteria.andEqualTo(Role::getDeleted, Role.DELETED_FALSE);

        // 关键字匹配
        if (!StringUtils.isEmpty(keyWord)) {
            // 封装查询信息(关键字的模糊匹配)
            WeekendCriteria<Role, Object> weekendKeywordCriteria = weekend.weekendCriteria();
            // TODO 待实现
            // 查询条件封装
            //weekendKeywordCriteria
            //    .orLike(User::getTelephone, WeekendCriteriaUtils.buildLikeParameter(keyWord));
            weekend.and(weekendKeywordCriteria);
        }
        return findPage(weekend, pageable);
    }
}
