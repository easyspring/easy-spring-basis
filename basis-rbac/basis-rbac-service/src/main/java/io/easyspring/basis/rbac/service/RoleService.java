package io.easyspring.basis.rbac.service;

import io.easyspring.basis.rbac.model.Role;
import io.easyspring.framework.base.service.BaseService;
import com.github.pagehelper.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

/**
 * 角色 服务类
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@Validated
public interface RoleService extends BaseService<Role> {

    /**
     * 根据传入的关键字和分页信息, 查询角色的分页数据
     *
     * @param keyWord 关键字
     * @param pageable 分页信息
     * @return com.github.pagehelper.Page<Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    Page<Role> findPageBuilder(String keyWord, Pageable pageable);
}
