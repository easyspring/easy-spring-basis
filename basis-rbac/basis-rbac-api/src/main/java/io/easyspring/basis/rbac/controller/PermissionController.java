package io.easyspring.basis.rbac.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

import io.easyspring.basis.rbac.model.Permission;
import io.easyspring.framework.base.pagehelper.PageInfo;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import io.easyspring.basis.rbac.service.PermissionService;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 资源 前端控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@RestController
@RequestMapping("/rbac/permission")
@Validated
@Api(value = "/rbac/permission", tags = "资源 API")
public class PermissionController {

    /**
     * 注入 资源 的 service
     */
    @Resource
    private PermissionService permissionService;


    /**
     * 资源 的添加方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "添加", notes = "资源 的添加方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PostMapping
    public Permission save(@NotNull(message = "资源 不能为空") @Valid
                                @RequestBody Permission permission){
        // 执行添加操作并返回保存后的对象
        return permissionService.insertSelective(permission);
    }

    /**
     * 根据传入的 id 删除对应的 资源
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "删除", notes = "根据传入的 id 删除对应的 资源")
    @DeleteMapping("/{id:\\d+}")
    public boolean delete(@PathVariable(value = "id") Long id) {
        // 执行删除并返回结果
        return permissionService.delete(id) > 0;
    }

    /**
     * 资源 的修改方法
     *
     * @param permission 资源
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "修改", notes = "资源的修改方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PutMapping
    public Permission update(@NotNull(message = "资源不能为空") @Valid
                                    @RequestBody Permission permission){
        // 执行添加操作并返回保存后的对象
        return permissionService.updateSelective(permission);
    }

    /**
     * 根据 id 查询单个 资源
     *
     * @param id 资源 id
     * @return io.easyspring.basis.rbac.model.Permission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "查询单个", notes = "根据 id 查询单个资源")
    @JsonView(CommonJsonView.DetailView.class)
    @GetMapping("/{id:\\d+}")
    public Permission get(@PathVariable(value = "id") Long id) {
        return permissionService.get(id);
    }

    /**
     * 根据条件, 查询 资源 集合信息
     *
     * @param permission 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping
    public List<Permission> list(Permission permission) {
        // 执行查询并返回相关数据
        return permissionService.list(permission);
    }

    /**
     * 查询 资源 分页信息
     *
     * @param permission 资源 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Permission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "分页查询", notes = "资源 分页信息")
    @JsonView(CommonJsonView.SimpleView.class)
    @GetMapping("/page")
    public PageInfo<Permission> findPage(Permission permission, Pageable pageable) {

        // 执行查询并返回结果
        Page<Permission> pageBuilder = permissionService.findPage(permission, pageable);

        // 返回查询结果
        return permissionService.buildPageInfo(pageBuilder);
    }


}

