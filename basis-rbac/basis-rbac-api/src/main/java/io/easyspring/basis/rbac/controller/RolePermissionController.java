package io.easyspring.basis.rbac.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

import io.easyspring.basis.rbac.model.RolePermission;
import io.easyspring.framework.base.pagehelper.PageInfo;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import io.easyspring.basis.rbac.service.RolePermissionService;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色-权限 前端控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@RestController
@RequestMapping("/rbac/role_permission")
@Validated
@Api(value = "/rbac/role_permission", tags = "角色-权限 API")
public class RolePermissionController {

    /**
     * 注入 角色-权限 的 service
     */
    @Resource
    private RolePermissionService rolePermissionService;


    /**
     * 角色-权限 的添加方法
     *
     * @param rolePermission 角色-权限
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "添加", notes = "角色-权限 的添加方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PostMapping
    public RolePermission save(@NotNull(message = "角色-权限 不能为空") @Valid
                                @RequestBody RolePermission rolePermission){
        // 执行添加操作并返回保存后的对象
        return rolePermissionService.insertSelective(rolePermission);
    }

    /**
     * 根据传入的 id 删除对应的 角色-权限
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "删除", notes = "根据传入的 id 删除对应的 角色-权限")
    @DeleteMapping("/{id:\\d+}")
    public boolean delete(@PathVariable(value = "id") Long id) {
        // 执行删除并返回结果
        return rolePermissionService.delete(id) > 0;
    }

    /**
     * 角色-权限 的修改方法
     *
     * @param rolePermission 角色-权限
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "修改", notes = "角色-权限的修改方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PutMapping
    public RolePermission update(@NotNull(message = "角色-权限不能为空") @Valid
                                    @RequestBody RolePermission rolePermission){
        // 执行添加操作并返回保存后的对象
        return rolePermissionService.updateSelective(rolePermission);
    }

    /**
     * 根据 id 查询单个 角色-权限
     *
     * @param id 角色-权限 id
     * @return io.easyspring.basis.rbac.model.RolePermission
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "查询单个", notes = "根据 id 查询单个角色-权限")
    @JsonView(CommonJsonView.DetailView.class)
    @GetMapping("/{id:\\d+}")
    public RolePermission get(@PathVariable(value = "id") Long id) {
        return rolePermissionService.get(id);
    }

    /**
     * 根据条件, 查询 角色-权限 集合信息
     *
     * @param rolePermission 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.RolePermission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping
    public List<RolePermission> list(RolePermission rolePermission) {
        // 执行查询并返回相关数据
        return rolePermissionService.list(rolePermission);
    }

    /**
     * 查询 角色-权限 分页信息
     *
     * @param rolePermission 角色-权限 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.RolePermission>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "分页查询", notes = "角色-权限 分页信息")
    @JsonView(CommonJsonView.SimpleView.class)
    @GetMapping("/page")
    public PageInfo<RolePermission> findPage(RolePermission rolePermission, Pageable pageable) {

        // 执行查询并返回结果
        Page<RolePermission> pageBuilder = rolePermissionService.findPage(rolePermission, pageable);

        // 返回查询结果
        return rolePermissionService.buildPageInfo(pageBuilder);
    }


}

