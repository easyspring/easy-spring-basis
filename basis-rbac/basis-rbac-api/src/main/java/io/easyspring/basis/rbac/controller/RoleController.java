package io.easyspring.basis.rbac.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.pagehelper.Page;
import io.easyspring.basis.rbac.model.Role;
import io.easyspring.basis.rbac.service.RoleService;
import io.easyspring.framework.base.pagehelper.PageInfo;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色 前端控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:09
 */
@RestController
@RequestMapping("/rbac/role")
@Validated
@Api(value = "/rbac/role", tags = "角色 API")
public class RoleController {

    /**
     * 注入 角色 的 service
     */
    @Resource
    private RoleService roleService;


    /**
     * 角色 的添加方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @ApiOperation(value = "添加", notes = "角色 的添加方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PostMapping
    public Role save(@NotNull(message = "角色 不能为空") @Valid
                                @RequestBody Role role){
        // 执行添加操作并返回保存后的对象
        return roleService.insertSelective(role);
    }

    /**
     * 根据传入的 id 删除对应的 角色
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @ApiOperation(value = "删除", notes = "根据传入的 id 删除对应的 角色")
    @DeleteMapping("/{id:\\d+}")
    public boolean delete(@PathVariable(value = "id") Long id) {
        // 执行删除并返回结果
        return roleService.delete(id) > 0;
    }

    /**
     * 角色 的修改方法
     *
     * @param role 角色
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @ApiOperation(value = "修改", notes = "角色的修改方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PutMapping
    public Role update(@NotNull(message = "角色不能为空") @Valid
                                    @RequestBody Role role){
        // 执行添加操作并返回保存后的对象
        return roleService.updateSelective(role);
    }

    /**
     * 根据 id 查询单个 角色
     *
     * @param id 角色 id
     * @return io.easyspring.basis.rbac.model.Role
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @ApiOperation(value = "查询单个", notes = "根据 id 查询单个角色")
    @JsonView(CommonJsonView.DetailView.class)
    @GetMapping("/{id:\\d+}")
    public Role get(@PathVariable(value = "id") Long id) {
        return roleService.get(id);
    }

    /**
     * 根据条件, 查询 角色 集合信息
     *
     * @param role 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @GetMapping
    public List<Role> list(Role role) {
        // 执行查询并返回相关数据
        return roleService.list(role);
    }

    /**
     * 查询 角色 分页信息
     *
     * @param role 角色 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.Role>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:09
     */
    @ApiOperation(value = "分页查询", notes = "角色 分页信息")
    @JsonView(CommonJsonView.SimpleView.class)
    @GetMapping("/page")
    public PageInfo<Role> findPage(Role role, Pageable pageable) {

        // 执行查询并返回结果
        Page<Role> pageBuilder = roleService.findPage(role, pageable);

        // 返回查询结果
        return roleService.buildPageInfo(pageBuilder);
    }


}

