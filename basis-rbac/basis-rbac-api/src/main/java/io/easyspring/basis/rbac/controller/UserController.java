package io.easyspring.basis.rbac.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

import io.easyspring.basis.rbac.model.User;
import io.easyspring.framework.base.pagehelper.PageInfo;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import io.easyspring.basis.rbac.service.UserService;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户 前端控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:08
 */
@RestController
@RequestMapping("/rbac/user")
@Validated
@Api(value = "/rbac/user", tags = "用户 API")
public class UserController {

    /**
     * 注入 用户 的 service
     */
    @Resource
    private UserService userService;


    /**
     * 用户 的添加方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @ApiOperation(value = "添加", notes = "用户 的添加方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PostMapping
    public User save(@NotNull(message = "用户 不能为空") @Valid
                                @RequestBody User user){
        // 执行添加操作并返回保存后的对象
        return userService.insertSelective(user);
    }

    /**
     * 根据传入的 id 删除对应的 用户
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @ApiOperation(value = "删除", notes = "根据传入的 id 删除对应的 用户")
    @DeleteMapping("/{id:\\d+}")
    public boolean delete(@PathVariable(value = "id") Long id) {
        // 执行删除并返回结果
        return userService.delete(id) > 0;
    }

    /**
     * 用户 的修改方法
     *
     * @param user 用户
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @ApiOperation(value = "修改", notes = "用户的修改方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PutMapping
    public User update(@NotNull(message = "用户不能为空") @Valid
                                    @RequestBody User user){
        // 执行添加操作并返回保存后的对象
        return userService.updateSelective(user);
    }

    /**
     * 根据 id 查询单个 用户
     *
     * @param id 用户 id
     * @return io.easyspring.basis.rbac.model.User
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @ApiOperation(value = "查询单个", notes = "根据 id 查询单个用户")
    @JsonView(CommonJsonView.DetailView.class)
    @GetMapping("/{id:\\d+}")
    public User get(@PathVariable(value = "id") Long id) {
        return userService.get(id);
    }

    /**
     * 根据条件, 查询 用户 集合信息
     *
     * @param user 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @GetMapping
    public List<User> list(User user) {
        // 执行查询并返回相关数据
        return userService.list(user);
    }

    /**
     * 查询 用户 分页信息
     *
     * @param user 用户 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.User>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:08
     */
    @ApiOperation(value = "分页查询", notes = "用户 分页信息")
    @JsonView(CommonJsonView.SimpleView.class)
    @GetMapping("/page")
    public PageInfo<User> findPage(User user, Pageable pageable) {

        // 执行查询并返回结果
        Page<User> pageBuilder = userService.findPage(user, pageable);

        // 返回查询结果
        return userService.buildPageInfo(pageBuilder);
    }


}

