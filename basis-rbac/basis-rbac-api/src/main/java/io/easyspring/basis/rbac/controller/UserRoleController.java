package io.easyspring.basis.rbac.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

import io.easyspring.basis.rbac.model.UserRole;
import io.easyspring.framework.base.pagehelper.PageInfo;
import io.easyspring.framework.base.view.CommonJsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import io.easyspring.basis.rbac.service.UserRoleService;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户-角色 前端控制器
 *
 * @author summer
 * @version V1.0.0-RELEASE
 * DateTime 2019-05-13 15:53:10
 */
@RestController
@RequestMapping("/rbac/user_role")
@Validated
@Api(value = "/rbac/user_role", tags = "用户-角色 API")
public class UserRoleController {

    /**
     * 注入 用户-角色 的 service
     */
    @Resource
    private UserRoleService userRoleService;


    /**
     * 用户-角色 的添加方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "添加", notes = "用户-角色 的添加方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PostMapping
    public UserRole save(@NotNull(message = "用户-角色 不能为空") @Valid
                                @RequestBody UserRole userRole){
        // 执行添加操作并返回保存后的对象
        return userRoleService.insertSelective(userRole);
    }

    /**
     * 根据传入的 id 删除对应的 用户-角色
     *
     * @param id 想要删除的数据的 id
     * @return boolean
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "删除", notes = "根据传入的 id 删除对应的 用户-角色")
    @DeleteMapping("/{id:\\d+}")
    public boolean delete(@PathVariable(value = "id") Long id) {
        // 执行删除并返回结果
        return userRoleService.delete(id) > 0;
    }

    /**
     * 用户-角色 的修改方法
     *
     * @param userRole 用户-角色
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "修改", notes = "用户-角色的修改方法")
    @JsonView(CommonJsonView.SimpleView.class)
    @PutMapping
    public UserRole update(@NotNull(message = "用户-角色不能为空") @Valid
                                    @RequestBody UserRole userRole){
        // 执行添加操作并返回保存后的对象
        return userRoleService.updateSelective(userRole);
    }

    /**
     * 根据 id 查询单个 用户-角色
     *
     * @param id 用户-角色 id
     * @return io.easyspring.basis.rbac.model.UserRole
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "查询单个", notes = "根据 id 查询单个用户-角色")
    @JsonView(CommonJsonView.DetailView.class)
    @GetMapping("/{id:\\d+}")
    public UserRole get(@PathVariable(value = "id") Long id) {
        return userRoleService.get(id);
    }

    /**
     * 根据条件, 查询 用户-角色 集合信息
     *
     * @param userRole 查询条件
     * @return java.util.List<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @GetMapping
    public List<UserRole> list(UserRole userRole) {
        // 执行查询并返回相关数据
        return userRoleService.list(userRole);
    }

    /**
     * 查询 用户-角色 分页信息
     *
     * @param userRole 用户-角色 查询条件
     * @param pageable 分页信息
     * @return io.easyspring.framework.base.pagehelper.PageInfo<io.easyspring.basis.rbac.model.UserRole>
     * Author summer
     * Version V1.0.0-RELEASE
     * DateTime 2019-05-13 15:53:10
     */
    @ApiOperation(value = "分页查询", notes = "用户-角色 分页信息")
    @JsonView(CommonJsonView.SimpleView.class)
    @GetMapping("/page")
    public PageInfo<UserRole> findPage(UserRole userRole, Pageable pageable) {

        // 执行查询并返回结果
        Page<UserRole> pageBuilder = userRoleService.findPage(userRole, pageable);

        // 返回查询结果
        return userRoleService.buildPageInfo(pageBuilder);
    }


}

